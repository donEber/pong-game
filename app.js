//Aqui validamos si el navegador del usuario soporta Service Worker :)
if (navigator.serviceWorker)
    navigator.serviceWorker.register('./sw.js');
let micanvas = document.getElementById('webglcanvas');
const glX = 11; // tamanio del X que definimos en webgl 
const glY = 11; // tamanio del Y que definimos en webgl 
let cvX = micanvas.width;
let cvY = micanvas.height;

micanvas.addEventListener('click', e => {
    let cvA = e.pageX - micanvas.offsetLeft;
    let cvB = e.pageY - micanvas.offsetTop;

    let glA = ((cvA * glX) / cvX) - (glX / 2);
    let glB = cambioDeDimensionY(cvB);
    cvX = micanvas.width;
    cvY = micanvas.height;    
    console.log('')
    console.log(`Posicion Cv: ( ${Math.round(cvA * 100) / 100}, ${Math.round(cvB * 100) / 100} )`)
    console.log(`Posicion GL: ( ${Math.round(glA * 100) / 100}, ${Math.round(glB * 100) / 100} )`)
    console.log('')
    // despX_r3 += incX_r3;
    // despX_r3 = 0 - (anchoRaqueta / 2);
    // despy_r3 = 0;
    mandoClick(glA);
});
micanvas.addEventListener('mousemove', e => {
    console.log('estas presionando...')
    let cvA = e.pageX - micanvas.offsetLeft;
    let cvB = e.pageY - micanvas.offsetTop;

    let glA = ((cvA * glX) / cvX) - (glX / 2);
    let glB = cambioDeDimensionY(cvB);
    cvX = micanvas.width;
    cvY = micanvas.height;    
    mandoClick(glA);
});
function mandoClick(puntoXGL){
    if(puntoXGL>(despX_r3+(anchoRaqueta/2)))
        despX_r3+=0.1;
    else
        despX_r3-=0.1;
}

function cambioDeDimensionX(puntoX) {
    let xGL = ((puntoX * glX) / cvX) - (glX );
    return xGL;
}
function cambioDeDimensionY(puntoY) {
    let yGL = glY - (puntoY * glY) / cvY - (glY / 2);
    return yGL;
}