//Este archivo controla los diferentes eventos (install, activate, fetch, etc.)
//Cada vez que se haga un cambio en este archivo se volvera a ejecutar el 'install' y el 'activate'

self.addEventListener('install', event => {
    //En esta etapa debemos descargar los assets, crear cache, etc
    //Todo lo que se deba hacer al iniciar nuestra aplicacion
    //La instalacion solo se realiza una vez (tomar en cuenta)
    console.log("SW: Instalando el Servie Worker...");

    //Estrategia del cache para uso OFFLINE
    //Creamos una vector que contiene la APP_SHELL (son todos los archivos necesarios para que corra la aplicacion)
    const APP_SHELL = [
        '/',
        // '/pong-game/',//para usar en 'GitLab Pages'
        'index.html',
        'app.js',
        'favicon.ico',
        'manifest.json',
        'icons/16x16.png',
        'icons/32x32.png',
        'icons/64x64.png',
        'icons/128x128.png',
        'icons/200x200.png',
        'webgl/clases.js',
        'webgl/funciones-variables.js',
        'webgl/matrices.js',
    ];
    const CACHE_STATIC_NAME = 'CACHE_STATIC_V1';    //guardamos el nombre de nuestro cache
    const promesaCache = caches.open(CACHE_STATIC_NAME)
        .then(cache => cache.addAll(APP_SHELL));    //creamos una promesa que crea y guarda el cache 
    event.waitUntil(promesaCache);  //esperamos que termine la promesa 'promesaCache' (es necesario esperar a que termine de instalarse la aplicacion)
})

self.addEventListener('activate', event => {
    //En este evento vemos la logica del cache
    console.log('SW: Activando el Service Worker...');
})

self.addEventListener('fetch', event => {
    //Cuando la aplicacion realiza una peticion HTTP para por este evento
    //Manipula las peticiones HTTP
    //Generalmente aqui hacemos uso de la estrategia del cache
    console.log('SW: ', event.request.url);
    //Estrategia del cache: 
    //1.- "Cache Only"
    //      Esta estrategia solo sirve si nunca se hara cambios en el proyecto, guarda una vez y consume toda la vida
    event.respondWith(caches.match(event.request));//Aqui en el fetch respondera con lo que tenemos en el cache guardado

})

/*
// Mientras tanto no usaremos estos eventos:
self.addEventListener('sync', event => {
    //Este evento observa si vuelve la conexion a internet
    //Mas detalles... PROXIMAMENTE
    console.log('SW: Tenemos conexion!');
})
self.addEventListener('push', event => {
    //Cuando se envia una notificacion push se ejecuta este evento
    console.log('SW: Notificacion recibida');
})
*/