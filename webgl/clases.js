/* Paso 4: Se renderizan los objetos                                       */
/***************************************************************************/
class RectanguloGrafico {
    constructor(gl, r, g, b,rectanguloAncho) {
        /**
        *    3 ---------- 2
        *     |        / |
        *     |      /   |
        *     |    /     |
        *     | /        |
        *    0 ---------- 1
        */

        /* Las coordenadas cartesianas (x, y) */
        var vertices = [
            0, 0, // 0
            //  -1,-.5,
            rectanguloAncho, 0, // 1
            //  1,-.5,
            rectanguloAncho, 1, // 2
            //  1,.5
            0, 1, // 3
            //  -1,.5,

        ];

        /* Lee los colores x v�rtice (r,g,b,a) */
        var colores = new Array(16);
        colores[0] = r; colores[1] = g; colores[2] = b; colores[3] = 1;
        colores[4] = r; colores[5] = g; colores[6] = b; colores[7] = 1;
        colores[8] = r; colores[9] = g; colores[10] = b; colores[11] = 1;
        colores[12] = r; colores[13] = g; colores[14] = b; colores[15] = 1;

        /* Se crea el objeto del arreglo de v�rtices (VAO) */
        this.rectanguloVAO = gl.createVertexArray();

        /* Se activa el objeto */
        gl.bindVertexArray(this.rectanguloVAO);


        /* Se genera un nombre (c�digo) para el buffer */
        var verticeBuffer = gl.createBuffer();

        /* Se asigna un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, verticeBuffer);

        /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

        /* Se habilita el arreglo de los v�rtices (indice = 0) */
        gl.enableVertexAttribArray(0);

        /* Se especifica el arreglo de v�rtices */
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);


        /* Se genera un nombre (c�digo) para el buffer */
        var colorBuffer = gl.createBuffer();

        /* Se asigna un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

        /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colores), gl.STATIC_DRAW);

        /* Se habilita el arreglo de los colores (indice = 1) */
        gl.enableVertexAttribArray(1);

        /* Se especifica el arreglo de colores */
        gl.vertexAttribPointer(1, 4, gl.FLOAT, false, 0, 0);


        /* Se desactiva el objeto del arreglo de v�rtices */
        gl.bindVertexArray(null);

        /* Se deja de asignar un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

    }

    dibuja(gl) {

        /* Se activa el objeto del arreglo de v�rtices */
        gl.bindVertexArray(this.rectanguloVAO);

        /* Se renderiza las primitivas desde los datos del arreglo */
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

        /* Se desactiva el objeto del arreglo de v�rtices */
        gl.bindVertexArray(null);

    }
}

class Rectangulo {

    // M�todos
    constructor(x, y, ancho, alto) {
        // Atributos
        this.x = x;
        this.y = y;
        this.ancho = ancho;
        this.alto = alto;
    }
    setX(x) {
        this.x = x;
    }
    setY(y) {
        this.y = y;
    }
    setAncho(ancho) {
        this.ancho = ancho;
    }
    setAlto(alto) {
        this.alto = alto;
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    getAncho() {
        return this.ancho;
    }
    getAlto() {
        return this.alto;
    }
}

class CirculoGrafico {
    constructor(gl, radio, r, g, b) {

        /**
        *             3      2
        *             
        *       4                  1
        *        	
        *    5                         0
        *    
        *       6                  9
        *        
        *             7      8		
        */

        /* Las coordenadas cartesianas (x, y) */
        var vertices = [];

        /* Los colores x c/v�rtice (r,g,b,a) */
        var colores = [];

        /* Lee los v�rtices (x,y) y colores (r,g,b,a) */
        for (var i = 0; i < 360; i++) {
            vertices.push(radio * Math.cos(i * Math.PI / 180));
            vertices.push(radio * Math.sin(i * Math.PI / 180));

            colores.push(r);
            colores.push(g);
            colores.push(b);
            colores.push(1);
        }

        /* Se crea el objeto del arreglo de v�rtices (VAO) */
        this.circuloVAO = gl.createVertexArray();

        /* Se activa el objeto */
        gl.bindVertexArray(this.circuloVAO);

        /* Se genera un nombre (c�digo) para el buffer */
        var verticeBuffer = gl.createBuffer();

        /* Se asigna un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, verticeBuffer);

        /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

        /* Se habilita el arreglo de los v�rtices (indice = 0) */
        gl.enableVertexAttribArray(0);

        /* Se especifica el arreglo de v�rtices */
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);

        /* Se genera un nombre (c�digo) para el buffer */
        var colorBuffer = gl.createBuffer();

        /* Se asigna un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

        /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colores), gl.STATIC_DRAW);

        /* Se habilita el arreglo de los colores (indice = 1) */
        gl.enableVertexAttribArray(1);

        /* Se especifica el arreglo de colores */
        gl.vertexAttribPointer(1, 4, gl.FLOAT, false, 0, 0);


        /* Se desactiva el objeto del arreglo de v�rtices */
        gl.bindVertexArray(null);

        /* Se deja de asignar un nombre (c�digo) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

    }

    dibuja(gl) {

        /* Se activa el objeto del arreglo de v�rtices */
        gl.bindVertexArray(this.circuloVAO);

        /* Se renderiza las primitivas desde los datos del arreglo */
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 360);

        /* Se desactiva el objeto del arreglo de v�rtices */
        gl.bindVertexArray(null);

    }
}

class Circulo {
    // M�todos
    constructor(x, y, radio) {
        // Atributos
        this.x = x;
        this.y = y;
        this.radio = radio;
    }
    setX(x) {
        this.x = x;
    }
    setY(y) {
        this.y = y;
    }
    setRadio(radio) {
        this.radio = radio;
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    getRadio() {
        return this.radio;
    }
}