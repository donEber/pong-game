const anchoRaqueta = 3.5;

// CONTROLA LA RAQUETA
let flechitas = function () {
    //DERECHA

    if (event.keyCode === 39 && despX_r3 < 4.5)
        despX_r3 += incX_r3;
    //ARRIBA
    if (event.keyCode === 38)
        despY_r3 += incY_r3;
    //IZQUIERDA
    if (event.keyCode === 37 && despX_r3 > -8)
        despX_r3 -= incX_r3;
    //ABAJO
    if (event.keyCode === 40)
        despY_r3 -= incY_r3;
}
function compilaEnlazaLosShaders() {

    /* Se compila el shader de vertice */
    var shaderDeVertice = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(shaderDeVertice, document.getElementById("vs").text.trim());
    gl.compileShader(shaderDeVertice);
    if (!gl.getShaderParameter(shaderDeVertice, gl.COMPILE_STATUS)) {
        console.error(gl.getShaderInfoLog(shaderDeVertice));
    }

    /* Se compila el shader de fragmento */
    var shaderDeFragmento = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(shaderDeFragmento, document.getElementById("fs").text.trim());
    gl.compileShader(shaderDeFragmento);
    if (!gl.getShaderParameter(shaderDeFragmento, gl.COMPILE_STATUS)) {
        console.error(gl.getShaderInfoLog(shaderDeFragmento));
    }

    /* Se enlaza ambos shader */
    programaID = gl.createProgram();
    gl.attachShader(programaID, shaderDeVertice);
    gl.attachShader(programaID, shaderDeFragmento);
    gl.linkProgram(programaID);
    if (!gl.getProgramParameter(programaID, gl.LINK_STATUS)) {
        console.error(gl.getProgramInfoLog(programaID));
    }

    /* Se instala el programa de shaders para utilizarlo */
    gl.useProgram(programaID);
}
function distancia2(x1, y1, x2, y2) {
    return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
}
function choque(c, r) {
    if (
        c.getX() + c.getRadio() > r.getX()
        && c.getX() - c.getRadio() < r.getX() + r.getAncho()
        && (c.getY() + c.getRadio()) < (r.getY() + r.getAlto() + 1)
        && (c.getY() - c.getRadio() > (r.getY()))
    ) {
        return true;
    }
    return false;
}
function choque2(c, r) {
    //Si esta en el intervalo X
    if ((c.getX()) > r.getX()
        && (r.getX() + r.getAncho()) > c.getX()) {

        //si esta arriba
        if ((c.getY() + c.getRadio()) < (r.getY() + r.getAlto())
            && (r.getY() - c.getRadio()) > (r.getY() + r.getAlto())) {
            return true
        }
        //si esta abajo
        if ((c.getY() - c.getRadio()) > r.getY()
            && (c.getY() + c.getRadio()) < r.getY())
            return true;
        return false;
    }
    return false;
}
function colisionCirculoRectangulo(c, r) {
    var x0 = c.getX();
    var y0 = c.getY();

    if (c.getX() < r.getX()) {
        x0 = r.getX();
    } else if (c.getX() > r.getX() + r.getAncho()) {
        x0 = r.getX() + r.getAncho();
    }

    if (c.getY() < r.getY()) {
        y0 = r.getY();
    } else if (c.getY() > r.getY() + r.getAlto()) {
        y0 = r.getY() + r.getAlto();
    }

    var d2 = distancia2(c.getX(), c.getY(), x0, y0);
    return d2 < c.getRadio() * c.getRadio();
}
function seSobreponenCR(c, r) {
    var x0 = c.getX();
    var y0 = c.getY();

    if (c.getX() < r.getX()) {
        x0 = r.getX();
    } else if (c.getX() >= r.getX() + r.getAncho()) {
        x0 = r.getX() + r.getAncho();
    }

    if (c.getY() < r.getY()) {
        y0 = r.getY();
    } else if (c.getY() >= r.getY() + r.getAlto()) {
        y0 = r.getY() + r.getAlto();
    }

    var d2 = distancia2(c.getX(), c.getY(), x0, y0);
    return d2 < c.getRadio() * c.getRadio();
}
function incPelotaYCuadrado() {
    if (incX_c3 > 0)
        incX_c3 += 0.00005;
    else
        incX_c3 -= 0.00005;
    if (incY_c3 > 0)
        incY_c3 += 0.00005;
    else
        incY_c3 -= 0.00005;

    incX_r3 += 0.00009;
    incY_r3 += 0.00009;
}
function dibuja() {
    //INREMENTO DE VELOCIDAD DE LA PELOTA
    incPelotaYCuadrado();
    /* Color de fondo */
    gl.clearColor(0.15, 0.55, 0.15, 1.0);

    /* Inicializa el buffer de color */
    gl.clear(gl.COLOR_BUFFER_BIT);

    /* Circulo 3 */
    identidad(MatrizModelo);
    traslacion(MatrizModelo, despX_c3, despY_c3, 0);
    c3.setX(despX_c3);
    c3.setY(despY_c3);
    gl.uniformMatrix4fv(uMatrizVistaModelo, false, MatrizModelo);
    circulo3.dibuja(gl);

    /* Rectangulo 3 */
    identidad(MatrizModelo);
    traslacion(MatrizModelo, despX_r3, despY_r3, 0);
    r3.setX(despX_r3);
    r3.setY(despY_r3);
    gl.uniformMatrix4fv(uMatrizVistaModelo, false, MatrizModelo);
    rectangulo3.dibuja(gl);
    //DIBUJA CADA CUADRADITO (LADRILLOS)
    for (let i = 0; i < numeroCuadrados; i++) {
        identidad(MatrizModelo);
        traslacion(MatrizModelo, posicionesX[i], posicionesY[i], 0);
        mC[i].setX(posicionesX[i]);
        mC[i].setY(posicionesY[i]);
        gl.uniformMatrix4fv(uMatrizVistaModelo, false, MatrizModelo);
        misCuadrados[i].dibuja(gl);
    }

    // CHOQUE CON RECTANGULO
    /*
    if (choque(c3, r3)) {
        incY_c3 = -incY_c3;
    }
    */

    if (colisionCirculoRectangulo(c3, r3)) {
        incY_c3 = -incY_c3;
        //posicion bolita
        despY_c3 = despY_r3 + 1.5;
    }
    //SI HAY COLICION CON ALGUN CUADRADO (se eliminaran)
    for (let i = 0; i < numeroCuadrados; i++) {

        if (colisionCirculoRectangulo(c3, mC[i])) {
            if (incX_c3 > 0)
                incX_c3 += 0.001;
            else
                incX_c3 -= 0.001;
            if (incY_c3 > 0)
                incY_c3 += 0.001;
            else
                incY_c3 -= 0.001
            mC.splice(i, 1);
            misCuadrados.splice(i, 1);
            posicionesX.splice(i, 1);
            posicionesY.splice(i, 1);
            numeroCuadrados--;
            //aniadimos al puntaje
            if (incY_c3 > 0)
                incY_c3 = -incY_c3;
            else
                incX_c3 = -incX_c3;
            break;
        }
    }
    //CHOQUE DE PAREDES DERECHA - IZQUIERDA            
    if (despX_c3 < -5 || despX_c3 > 5) {
        incX_c3 = -incX_c3;
    }
    despX_c3 = despX_c3 + incX_c3;
    //CHOQUE DE PAREDES ARRIBA - ABAJO
    if (despY_c3 < -5 || despY_c3 > 5) {
        incY_c3 = -incY_c3;
    }
    despY_c3 = despY_c3 + incY_c3;

    /* Solicita que el navegador llame nuevamente a dibuja */
    //SI PERDIO, ENTONCES YA FUE
    if (c3.getY() > -5) {
        requestAnimationFrame(dibuja, canvas);
    } else {
        //posicion bolita
        despX_c3 = 0;
        despY_c3 = -2;
        //posicion del rectangulo
        despY_r3 = -5, despX_r3 = 1;
        //Velocidad del rectangulo
        incX_r3 = 0.3, incY_r3 = 0.3;
        // Velocidad de la bolita
        incY_c3 = -0.035, incX_c3 = 0.045;
    }
    if (numeroCuadrados === 0) {
        alert("NIVEL " + nroFilas + " COMPLETADO");
        nroFilas++;
        miCont2 = 4;
        //posicion de la bolita
        despX_c3 = 0;
        despY_c3 = 0;
        incY_c3 = -0.035, incX_c3 = 0.045;//reinicion de velocidad
        numeroCuadrados = nC * nroFilas;
        despY_r3 = -5, despX_r3 = 1;
        creaPosiciones(nroFilas, numeroCuadrados);
        creaCuadrados(numeroCuadrados);
    }
}
function creaPosiciones(nFilas, nCuadrados) {
    for (let j = 0; j < nFilas; j++) {
        if (j % 2 == 0)
            miCont = -4.8;
        else
            miCont = -5.4;
        for (let i = 0; i < nCuadrados / nFilas; i++) {
            posicionesX.push(parseFloat(miCont.toFixed(1)));
            posicionesY.push(miCont2);
            miCont += 2.1;
        }
        miCont2 -= 1.1;
    }
}
function creaCuadrados(numCuadrados) {
    //CREANDO LOS CUADRADOS Y SUS GRAFICOS
    for (let i = 0; i < numCuadrados; i++) {
        let car = parseFloat(Math.random().toFixed(1));
        let cag = parseFloat(Math.random().toFixed(1));
        let cab = parseFloat(Math.random().toFixed(1));
        //misCuadrados[i] = new RectanguloGrafico(gl, car, cag, cab, 2);//2 es el ancho
        if (i % 3 == 0)
            misCuadrados[i] = new RectanguloGrafico(gl, 255 / 255, 127 / 255, 80 / 255, 2);//2 es el ancho
        else
            if (i % 3 == 1)
                misCuadrados[i] = new RectanguloGrafico(gl, 245 / 255, 115 / 255, 70 / 255, 2);//2 es el ancho
            else
                misCuadrados[i] = new RectanguloGrafico(gl, 235 / 255, 105 / 255, 60 / 255, 2);//2 es el ancho
        mC[i] = new Rectangulo(0, 0, 2, 1);
    }
}
function main() {
    canvas = document.getElementById("webglcanvas");

    gl = canvas.getContext("webgl2");
    if (!gl) {
        document.write("WebGL 2.0 no est� disponible en tu navegador");
        return;
    }
    // Se define el cambio de escala para no perder calidad
    var s = getComputedStyle(canvas);
    var w = s.width;
    var h = s.height;
    canvas.width=w.split("px")[0];
    canvas.height=h.split("px")[0];

    /* Se define la ventana de despliegue */
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    compilaEnlazaLosShaders();

    circulo3 = new CirculoGrafico(gl, 0.5, 1, 1, 1);
    c3 = new Circulo(0, 0, 0.5);
    //RectanguloGrafico( <gl>, <colorR> , <colorG> , <colorB>, <Ancho>  )
    rectangulo3 = new RectanguloGrafico(gl, .2, .2, .5, anchoRaqueta);
    //Rectangulo( <ejeX>, <ejeY>, <Ancho>, <Alto>, )
    r3 = new Rectangulo(0, 0, anchoRaqueta, 1);
    creaPosiciones(nroFilas, numeroCuadrados)
    creaCuadrados(numeroCuadrados);

    /* Obtiene los ID de las variables de entrada de los shaders */
    gl.useProgram(programaID);
    uMatrizProyeccion = gl.getUniformLocation(programaID, "uMatrizProyeccion");
    uMatrizVistaModelo = gl.getUniformLocation(programaID, "uMatrizVistaModelo");

    /* Proyecci�n Paralela */
    ortho(MatrizProyeccion, -5.5, 5.5, -5.5, 5.5, -5.5, 5.5);
    gl.uniformMatrix4fv(uMatrizProyeccion, false, MatrizProyeccion);

    dibuja();
}
var canvas;
var programaID;
var gl;
// NUMERO DE CUADRADITOS
var nC = 5;
var nroFilas = 1;
var numeroCuadrados = nC * nroFilas;

var misCuadrados = new Array(numeroCuadrados);
var mC = new Array(numeroCuadrados);
var miCont;
var miCont2 = 4;
var posicionesX = [];
var posicionesY = [];

var circulo3;
var c3;

var rectangulo3;
var r3;

var MatrizModelo = new Array(16);
var MatrizVistaModelo = new Array(16);
var MatrizProyeccion = new Array(16);

var uMatrizProyeccion;
var uMatrizVistaModelo;
//posicion bolita
var despX_c3 = 0;
var despY_c3 = -2;
//posicion del rectangulo
var despY_r3 = -5, despX_r3 = 1;
//Velocidad del rectangulo
var incX_r3 = 0.3, incY_r3 = 0.3;
// Velocidad de la bolita
var incY_c3 = -0.035, incX_c3 = 0.045;

window.onload = main;